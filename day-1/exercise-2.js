import {my_display_alpha} from './exercise-1.js';

export const my_display_alpha_reverse = () => {
    let alphabet = my_display_alpha();
    let reverseAlphabet = "";
    let length = 0;
    while (alphabet[length] !== undefined){
        length++;
    }
    for(let i = length-1; i >= 0; i-=2){
        if(i != 0){
            reverseAlphabet += alphabet[i]+',';
        }else{
            reverseAlphabet += alphabet[i];
        }
    }
    return reverseAlphabet;
};