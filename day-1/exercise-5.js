import {my_size_alpha} from './exercise-4.js';

export const my_array_alpha = (str) => {
    let lengthStr = my_size_alpha(str);
    let arrayReturn = [];
    for(let i = 0; i <= lengthStr-1; i++){
        arrayReturn[i] = str[i];
    }
    //console.log(arrayReturn);
    return arrayReturn;
};