export const my_is_posi_neg = (nbr) =>{
    let stringReturn;

    if(nbr < 0){
        stringReturn = "NEGATIF";
    }else if(nbr > 0 || nbr == undefined || nbr == null){
        stringReturn = "POSITIF"
    }else if(nbr == 0){
        stringReturn = "NEUTRAL";
    }else{
        stringReturn = "PARAMETER VALUE IS NOT CORRECT"
    }
    
    return stringReturn;
}