import { expect } from 'chai';
import {my_is_posi_neg} from '../day-1/exercise-7.js';

describe('day-1',()=>{
    describe('exercise-7 my_is_posi_neg(nbr)', () =>{
        it('should display positif for parameter value equal to 5',()=>{
            expect(my_is_posi_neg(5)).to.equal("POSITIF");
        });
        it('should display negatif for parameter value equal to -5',()=>{
            expect(my_is_posi_neg(-5)).to.equal("NEGATIF");
        });
        it('should display positif for parameter value equal to undefined',()=>{
            expect(my_is_posi_neg(undefined)).to.equal("POSITIF");
        });
        it('should display neutral for parameter value equal to 0',()=>{
            expect(my_is_posi_neg(0)).to.equal("NEUTRAL");
        });
    });
});