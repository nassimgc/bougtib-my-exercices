import { expect } from 'chai';
import {my_array_alpha} from '../day-1/exercise-5.js';

describe('day-1',()=>{
    describe('exercise-5 my_array_alpha(str)', () =>{
        it('should give array of each characters from string here : [n, a, s, s, i, m]',()=>{
            expect(my_array_alpha("nassim")).to.eql(['n', 'a', 's', 's', 'i', 'm']);
        });

        it('should give array of each characters from string here : [t, r, u, c]',()=>{
            expect(my_array_alpha("truc")).to.eql(['t', 'r', 'u', 'c']);
        });

        it('should give array of each characters from string here : [P, a, r, i, s, , 1, 9]',()=>{
            expect(my_array_alpha("Paris 19")).to.eql(['P', 'a', 'r', 'i','s',' ','1','9']);
        });
    });
});