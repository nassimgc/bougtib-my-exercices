import { expect } from 'chai';
import {my_alpha_number} from '../day-1/exercise-3.js';

describe('day-1',()=>{
    describe('exercise-3 my_alpha_number()', () =>{
        it('should convert number to string for parameter value 5',()=>{
            expect(my_alpha_number(5)).to.equal('5');
        });

        it('should convert number to string for parameter value \'7\' ',()=>{
            expect(my_alpha_number('7')).to.equal('7');
        });

        it('should convert number to string for parameter value 0 ',()=>{
            expect(my_alpha_number(0)).to.equal('0');
        });

        it('should convert number to string for parameter value -5 ',()=>{
            expect(my_alpha_number(-5)).to.equal('-5');
        });
    });
});