import { expect } from 'chai';
import {my_display_alpha_reverse} from '../day-1/exercise-2.js';

describe('day-1',()=>{
    describe('exercise-2 my_display_alpha_reverse()', () =>{
        it('should compute reverse alphabet from z to a',()=>{
            expect(my_display_alpha_reverse()).to.equal("z,y,x,w,v,u,t,s,r,q,p,o,n,m,l,k,j,i,h,g,f,e,d,c,b,a");
        });
    });
});