import { expect } from 'chai';
import {my_length_array} from '../day-1/exercise-6.js';

describe('day-1',()=>{
    describe('exercise-6 my_length_array(arr)', () =>{

        it('should give array length : here 1',()=>{
            expect(my_length_array(["test"])).to.equal(1);
        });
        it('should give array length : here 5',()=>{
            expect(my_length_array([0,7,8,5,"lolo"])).to.equal(5);
        });

        it('should give array length : here 2',()=>{
            expect(my_length_array(["test", true])).to.equal(2);
        });

        it('should give array length : here 0',()=>{
            expect(my_length_array([])).to.equal(0);
        });
    });
});