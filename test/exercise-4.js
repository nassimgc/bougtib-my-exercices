import { expect } from 'chai';
import {my_size_alpha} from '../day-1/exercise-4.js';

describe('day-1',()=>{
    describe('exercise-4 my_size_alpha(str)', () =>{
        it('should give string length : here 6',()=>{
            expect(my_size_alpha("nassim")).to.equal(6);
        });

        it('should give string length : here 3',()=>{
            expect(my_size_alpha("PSG")).to.equal(3);
        });

        it('should give string length : here 0',()=>{
            expect(my_size_alpha(true)).to.equal(0);
        });

        it('should give string length : here 10',()=>{
            expect(my_size_alpha(10)).to.equal(0);
        });
    });
});